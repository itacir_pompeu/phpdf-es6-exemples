'use strict'

const getUser = async function () {  
  return new Promise((resolve, reject)=>{
    setTimeout(_=>{
      resolve({ name: 'Pompeu' })
    }, 1000)
  })
}

const getUserAddr = async function (user) {  
  return new Promise((resolve, reject)=>{
    setTimeout(_=>{
      resolve({
        name:  user.name,
        adrr: 'cln 116'
      })
    }, 500)
  })
}

async function getUserFullData(){  
  let userData = await getUser()
  let userAddress = await getUserAddr(userData)
  const {name, adrr} = userAddress
  console.log(name, adrr);
}

getUserFullData()  
