'use strict';

const co = require('co')
const ajaxSimulator = require('./ajax.sumulator')

co(function * () {
  const packjson = yield ajaxSimulator('./package.json')
  console.log(packjson)
})

co(function * () {
  try {
    const packjson = yield ajaxSimulator('./a.json')
    console.log(packjson)
  } catch (e) {
    console.log(e);
  }
})
