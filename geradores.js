'use strict';

// while(true)

function willBeBroken () {
  let i = 0;
  while (true)  {
    console.log(++i); 
  }
}

console.log(willBeBroken());
console.log('FALA GALERAAAAAAAAA!!');

// generator 
/*
function * gen () {
  let i = 0;

  while(true) {
    yield ++i; 
  }
}

const res = gen();

console.log(res.next());
console.log(res.next());
console.log(res.next());
console.log(res.next());

for(const i of gen()) {
  console.log(i);
}
*/
