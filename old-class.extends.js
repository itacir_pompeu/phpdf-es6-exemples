'use strict';

function Animal (name) {
  this.name = name 

  this.toString = function () {
    return 'name of animal is ' + this.name
  }
}

const Dog = function (name) {
  Animal.call(this, name);
};

Dog.prototype = new Animal();
Dog.prototype.constructor = Dog;

Dog.prototype.walk = function () {
  console.log('waking ...');
}

const Fish = function (name) {
  Animal.call(this, name);
};

Fish.prototype = new Animal()
Fish.prototype.constructor = Fish;

Fish.prototype.swin = function () {
  console.log('swing ...');
}

const dog = new Dog('Thor')
const fish = new Fish('Thanos')
const animal = new Animal('Jakiro')

console.log(dog);
console.log(fish);
console.log(animal);
