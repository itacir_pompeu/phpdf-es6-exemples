'use strict';

const fib = n => {
  if (n <= 1) {
    return n 
  }

  return fib(n - 1) + fib(n - 2)
}



console.log(fib(10));
// 55
/*
const fib_inline = n => n <= 1 ? n : (fib_inline(n -1)  + fib_inline(n - 2))
console.log(fib_inline(8));
// 28
*/
