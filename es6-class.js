'use strict';


class Animal {
  constructor (name) {
    this.name = name 
  } 

  toString () {
    return `the name of animal is ${this.name}`
  }
}

class Dog extends Animal {
  constructor (name) {
    super(name) 
  }

  walk () {
    console.log('waking ...'); 
  }  
}


class Fish extends Animal {
  constructor (name) {
    super(name) 
  }

  swin () {
    console.log('swing ...'); 
  } 
}

const dog = new Dog('Thor')
const fish = new Fish('Thanos')
const animal = new Animal('Jakiro')

console.log(dog);
console.log(fish);
console.log(animal);
