'use strict';

const fs = require('fs');

module.exports = path => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf-8', (err, file) => {
      err ? reject(err) : resolve(file) 
    })
  })
}
